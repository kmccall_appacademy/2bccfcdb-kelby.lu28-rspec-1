def latinize(word)
  vowels = "aeiou"

  while !vowels.include?(word[0]) || (word[0] == "u" && word[-1] == "q")
    word = word[1..-1] + word[0]
  end

  word += "ay"
end

def translate(string)
  latinized_words = string.split(" ").map {|word| latinize(word)}

  latinized_words.join(" ")
end
