def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array_of_num)
  array_of_num.reduce(0, :+)
end

def multiply(num1, num2)
  num1 * num2
end

def power(num, power)
  num**power
end

def factorial(num)
  (0..num).reduce(:+)
end
