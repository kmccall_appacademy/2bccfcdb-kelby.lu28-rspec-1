def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, repeats = 2)
  ("#{string} " * repeats).strip
end

def start_of_word(string, num_letters = 1)
  string[0...num_letters]
end

def first_word(string)
  string.split(" ")[0]
end

def titleize(string)
  little_words = ["over", "the", "and"]
  titled_words = []

  string.split(" ").each_with_index do |word, idx|
    if little_words.include?(word) && idx != 0
      titled_words << word
    else
      titled_words << word.capitalize
    end
  end

    titled_words.join(' ')
end
